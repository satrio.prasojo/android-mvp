package com.akag.training00.presenter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.akag.training00.activities.LoginActivity;
import com.akag.training00.api.ApiClient;
import com.akag.training00.interfaces.ApiInterface;

public class BasePresenter {
    public SharedPreferences pref;

    public Activity activity;

    public ApiInterface apiInterface;

    public BasePresenter(){

    }

    public void initSharedPreferences(){
        pref = activity.getApplicationContext().getSharedPreferences("pref", 0);
        Toast.makeText(activity.getApplicationContext(), "Shared Preference Initialized!", Toast.LENGTH_SHORT).show();
    }

    public boolean checkLoginState(){
        boolean isLogin = false;

        if(pref.contains("isLoggedIn")){
            Boolean isLoggedIn = pref.getBoolean("isLoggedIn", true);
            if(isLoggedIn){
                // klo udah login
                isLogin = true;
            }
        }

        return isLogin;
    }

    public void initApi(){
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Toast.makeText(activity, "api initialized", Toast.LENGTH_SHORT).show();
    }
}
