package com.akag.training00.interfaces;

public interface LoginView {

    void updateTextState(String info);
    void showBothError();
    void showErrorUsername(Integer code);
    void showErrorPassword(Integer code);
    void valid();
}
